#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "support.h"

void *hello(void *input) {
  printf("hello from a .so\n");
  return NULL;
}

//loop through each name and email and if the character is lower case, convert it to upper
//by subtracting 32 from its ascii value
void *ucase(struct team_t *input) {
  struct team_t *teamstruct = malloc(sizeof(struct team_t));
  //name1
  int i;
  int c = 0;
  char name1[strlen(input->name1)];
  strcpy(name1,input->name1);
  while (name1[c] != '\0') {
    if (name1[c] >= 'a' && name1[c] <= 'z') {
      name1[c] = name1[c] - 32;
    }
    c++;
  }
  name1[c]='\0';

	//name2
  char name2[strlen(input->name2)];
  strcpy(name2,input->name2);

  int c2 = 0;
  while (name2[c2] != '\0') {
    if (name2[c2] >= 'a' && name2[c2] <= 'z') {
      name2[c2] = name2[c2] - 32;
    }
    c2++;
  }
  name2[c2]='\0';

  //name3
  char name3[strlen(input->name3)];
  strcpy(name3, input->name3);

  int c5 = 0;
  while (name3[c5] != '\0') {
    if (name3[c5] >= 'a' && name3[c5] <= 'z') {
      name3[c5] = name3[c5] -32;
    }
    c5++;
  }
  name3[c5] = '\0';


	//email1
  char email1[strlen(input->email1)];
  strcpy(email1,input->email1);

  int c3 = 0;
  while (email1[c3] != '\0') {
    if (email1[c3] >= 'a' && email1[c3] <= 'z') {
      email1[c3] = email1[c3] - 32;
    }
    c3++;
  }
  email1[c3]='\0';

  //email2
  char email2[strlen(input->email2)];
  strcpy(email2,input->email2);

  int c4 = 0;
  while (email2[c4] != '\0') {
    if (email2[c4] >= 'a' && email2[c4] <= 'z') {
      email2[c4] = email2[c4] - 32;
    }
    c4++;
  }
  email2[c4]='\0';

  //email3
  char email3[strlen(input->email3)];
  strcpy(email3,input->email3);
  int c6 = 0;
  while (email3[c6] != '\0') {
    if (email3[c6] >= 'a' && email2[c6] <= 'z') {
      email3[c6] = email3[c6] - 32;
    }
    c6++;
  }
  email3[c6]='\0';


  teamstruct->name1 = name1;
  teamstruct->name2 = name2;
  teamstruct->name3 = name3;
  teamstruct->email1 = email1;
  teamstruct->email2 = email2;
  teamstruct->email3 = email3;

  printf("Student 1 : %s\n Email 1 :%s\n Student 2 : %s\n Email 2 : %s\n Student 3 : %s\n Email 3 :%s\n",name1,email1,name2,email2,name3,email3);
  return teamstruct;
}
