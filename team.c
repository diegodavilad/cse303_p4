#include"support.h"

/* Please be sure to fill in this struct right away! */
struct team_t team =
{
	"Anna Malisova", /* first member name   */
	"adm418", /* first member email  */
	"Diego Davila", /* second member name  */
	"djr618", /* second member email */
	"Will Silverstone", /* third member name   */
	"wes219", /* third member email  */
};
